package au.edu.csu.ocb.sparktest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static spark.Spark.*;

/**
 * Created by jnagle03 on 13/04/2017.
 */


public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class.getName());

    public static void main(String[] args) {
        LOG.debug("Foo Bar");
        get("/hello", (req, res) -> "Hello World");
    }
}
